import axios from "axios"

export const resolvers = {
      Query: { 
            personCount: async (root, args) => {
                  const { data: personFromRestApi } = await axios.get('http://localhost:3000/Usuario')
                  return personFromRestApi.length
            },

            findUserByDni: async (root, args) => {
                  const { data: usuarioLista } = await axios.get('http://localhost:3000/Usuario')
                  const { dni } = args

                  if ((usuarioLista.find(persons => persons.dni == args.dni))) {
                        return usuarioLista.find(personFromRestApi => personFromRestApi.dni == dni)
                  } else {
                        console.log("\nNo se encuentran relaciones con ese parametro\nParametro ingresado: ", dni)
                  }
            }
      }
}