import { gql } from "apollo-server"

export const typeDefs = gql`


      type Perfil_usuario {
            perfil_inversion: String
            nivel_usuario: String!
      }

      type Usuario {
            gastos: String
            id_usuario: String!
            nombre: String!
            dni: String!
            tarjeta: String!
            clave: String!
            balance: Balance
            cuenta: [Cuenta]
      }

      type Balance {
            movimiento: [Movimiento]
            num_movimiento: Int
            ingreso: Int!
            balance_geneal: Int
            perfil_Usuario: Perfil_usuario!
            cuenta: [Cuenta]
      }

      type Movimiento {
            mes: String
            categoria: String
            porcentaje: Int
            consumo: Int!
            fecha: String!
      }

      type Cuenta {
            nombre: String!
            tipo_cuenta: String!
      }

       
      type Query {
            personCount: Int!
            findUserByDni(dni: String!): Usuario
      }
`
