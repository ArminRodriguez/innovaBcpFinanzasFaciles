import { ApolloServer } from "apollo-server";
import {typeDefs} from "../schema/schemas.js"
import { resolvers } from "../service/user-detail.js";


async function startAppServer(){
      const server = new ApolloServer({
            typeDefs, resolvers
      })

      await server.listen().then(({url}) => {
            console.log(`Server ready at ${url}`)

      })
}

startAppServer()