import React from 'react'

function Footer() {
  return (
    <div className='footer d-flex flex-column justify-content-center align-items-center'>
      <div className='d-flex footer_top text-center justify-content-center '>
        <img src={ require('../assets/images/candado_login.png') } className='candado' alt="candado" />
        <div>
          Esta es una pagina segura del BCP. Si tienes dudas sobre la autenticidad de la web, comunicate con nosotros al 311-9898 o a traves de nuestros medios digitales.
        </div>
      </div>
      <div className='d-flex justify-content-between align-items-center gap-4 text-break'>
        <div className='footer_item'>Banco de Credito del Peru S A - RUC: 2o10oo47218</div>
        <div className='footer_item'>Todos los derechos reservados</div>
        <div className='footer_item'>O 2023 VIABCP.com</div>
      </div>
    </div>
  )
}

export default Footer