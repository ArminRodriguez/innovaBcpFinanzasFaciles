import React, {useState } from 'react'
import './login.css'
import Footer from './Footer'
import SectionLeft from './SectionLeft'
import UserDetail from '../service/UserDetail'

import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { useNavigate } from 'react-router-dom'

const GET_USER_BY_DNI = gql`
  query FindUserByDni($dni: String!) {
    findUserByDni(dni: $dni) {
      tarjeta
      nombre
      clave
    }
  }
`;


function Login() {

  const navigate = useNavigate();

  const [tipoDocumento, setTipoDocumento] = useState('dni'); // Valor por defecto para el tipo de documento
  const [numeroDocumento, setNumeroDocumento] = useState('');
  const [numeroTarjeta, setNumeroTarjeta] = useState('');
  const [clave, setClave] = useState('');
  const [codigo, setCodigo] = useState('');
  const [respuesta, setRespuesta] = useState(''); // Valor por defecto para la respuesta del servicio

  const [errorNumeroDocumento, setErrorNumeroDocumento] = useState(false);
  const [errorNumeroTarjeta, setErrorNumeroTarjeta] = useState(false);
  const [errorClave, setErrorClave] = useState(false);
  const [errorCodigo, setErrorCodigo] = useState(false);

  // Función para validar el número de documento
  const validateNumeroDocumento = () => {
    if (tipoDocumento === 'dni' && numeroDocumento.length !== 7) {
      setErrorNumeroDocumento('El DNI debe tener 8 dígitos');
    } else if (tipoDocumento === 'ruc' && numeroDocumento.length !== 11) {
      setErrorNumeroDocumento('El RUC debe tener 11 dígitos');
    } else {
      setErrorNumeroDocumento('');
    }
  };

  // Función para validar el número de tarjeta
  const validateNumeroTarjeta = () => {
    if (numeroTarjeta.length !== 15) {
      setErrorNumeroTarjeta('La tarjeta debe tener 16 dígitos');
    } else {
      setErrorNumeroTarjeta('');
    }
  };

  // Función para validar la clave
  const validateClave = () => {
    if (clave.length !== 5) {
      setErrorClave('La clave debe tener 6 dígitos');
    } else {
      setErrorClave('');
    }
  };

  // Función para validar el código
  const validateCodigo = () => {
    if (codigo.length !== 5) {
      setErrorCodigo('El código debe tener 6 dígitos');
    } else {
      setErrorCodigo('');
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setRespuesta(<UserDetail dni={numeroDocumento} />)
    // Navegar hasta la ruta /zenSpace
    // const navigate = useNavigate();
    // navigate('/zenSpace');
  }

  return (

    <div id='container'>
    
      <SectionLeft />
      <section className='section-right p-2'>
        <div className='text-secondary text-right'>
          <p className=''>Esta ventana se cerrará en 297 segundos</p>
        </div>
        <div>
          <button className='volver-button'>
            <img src={ require('../assets/images/left_arrow.png') } alt="arrow left" />
            Volver
          </button>
        </div>

        {/* formulario */}
        <div className='p-4'>
          <form action="" className='d-flex flex-column gap-4' onSubmit={handleSubmit}>
            <h2 className='title-form text-center'>Banca por Internet</h2>
            <div className='d-flex gap-4'>
              <div className="form-check">
                <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked/>
                <label className="form-check-label" for="flexRadioDefault1">
                  Persona
                </label>
              </div>
              <div className="form-check">
                <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2"/>
                <label className="form-check-label" for="flexRadioDefault2">
                  Empresa
                </label>
              </div>
            </div>
            
            <div className='form-group d-flex gap-2'>
              <select name="" id="" className='form-select w-25' value={tipoDocumento} onChange={(e) => { setTipoDocumento(e.target.value); validateNumeroDocumento(); }}>
                <option value="dni">DNI</option>
                <option value="ruc">RUC</option>
              </select>
              <input type="text" className='form-control' placeholder='Nro de documento' onChange={(e) => { setNumeroDocumento(e.target.value); validateNumeroDocumento(); }} />
            </div>
            {/* Mensaje de error para el número de documento */}
            {errorNumeroDocumento && <div className="errmsg">{errorNumeroDocumento}</div>}
            
            <div className="form-group">
              {/* <label htmlFor="tarjeta">Número de tarjeta</label> */}
              <input type="text" className='form-control' id='tarjeta' maxLength={16} placeholder='Número de tarjeta' onChange={(e) => { setNumeroTarjeta(e.target.value); validateNumeroTarjeta(); }} />
            </div>

            {/* Mensaje de error para el número de tarjeta */}
            {errorNumeroTarjeta && <div className="errmsg">{errorNumeroTarjeta}</div>}

             {/* check button  */}
            <div className='d-flex'>
              <div className='form-check'>
                <input type="checkbox" className='form-check-input' id='check' checked />
                <label htmlFor="check" className='form-check-label'>Recordar datos</label>
              </div>
            </div>
            
            <div className='form-group'>
              {/* <label htmlFor=""></label> */}
              <input type="password" className='form-control' name="" id="" maxLength={6} placeholder='Clave de internet de 6 dígitos' onChange={(e) => { setClave(e.target.value); validateClave(); }} />
            </div>
            {/* Mensaje de error para la clave */}
            {errorClave && <div className="errmsg">{errorClave}</div>}

            <div className='d-flex justify-content-between input_options'>
              <span>Crear clave</span>
              <span>Olvide mi clave</span>
            </div>

            <div className="form-group d-flex gap-1 justify-content-center align-items-center">
              <img src={require("../assets/images/codigo_login.png")} className='w-25' alt="" />
              <input type="text" className='form-control' placeholder='Codigo' onChange={(e) => { setCodigo(e.target.value); validateCodigo(); }} />
            </div>
            {/* Mensaje de error para el código */}
            {errorCodigo && <div className="errmsg">{errorCodigo}</div>}
            
            <button type='submit' className='submit-button' onClick={()=>navigate("/zenSpace")}>
              Continuar
            </button>
            
          </form>
        </div>       

        {/* Footer */}
        <Footer />
      </section>
    </div>
  )
}

export default Login