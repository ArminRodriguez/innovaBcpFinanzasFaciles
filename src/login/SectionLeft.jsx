import React from 'react'

function SectionLeft() {
  return (
    <section className='section-left d-flex flex-column g-3 p-2'>
      <figure className='mb-4'>
        <img src={ require('../assets/images/bcp-logo-login.png') } className='bcp_logo w-25' alt="bcp logo" />
      </figure>
      <div className='mt-4'>
        <figure>
          <img src={ require('../assets/images/imagen_login.png') } alt="bcp image login" />
        </figure>
      </div>
    </section>
  )
}

export default SectionLeft