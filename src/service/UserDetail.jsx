import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

//Prueben esto
const GET_USER_BY_DNI = gql
  `query FindUserByDni($dni: String!) {
    findUserByDni(dni: $dni) {
      nombre
      tarjeta
      clave
      dni
      balance {
        movimiento {
          categoria
          mes
          porcentaje
        }
      }
    }
  }
`;

// <p><UserDetail dni={"11111111"}></UserDetail></p> Asi tendria que usarse

function UserDetail({ dni }) {
  const { data, error, loading } = useQuery(GET_USER_BY_DNI, {
    variables: { dni },
  });

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const user = data.findUserByDni;

  //Lo retorne asi para ver, puedes cambiar el orde o solo retornar user y agarrar los valores de ahi
  return (
    <div>
      <h2>Detalles del Usuario</h2>
      <p>Tarjeta: {user.tarjeta}</p>
      {/* <p>gastos: {user.gastos}</p> */}
      <p>Nombre: {user.nombre}</p>
      <p>Clave: {user.clave}</p>
    </div>
  );
}

export default UserDetail;