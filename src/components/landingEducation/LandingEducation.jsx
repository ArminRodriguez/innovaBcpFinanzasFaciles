import React from 'react';
import './LandingEducation.css';
import beneficiosImage from '../../assets/image_espacio.png';
import {} from 'react-bootstrap';
function LandingEducation() {
    return (
        <div className="education-section">
            <div className="education-text">
                <h2>Aprende y
                    prueba tus
                    conocimientos</h2>
                <p>Ir al Campus ABC</p>
            </div>
            <div className="education-image m-3">
                <img src={beneficiosImage} alt="Beneficios BCP"  width="300" height="300"/>
            </div>
            <div className="education-image m-3">
                <img src={beneficiosImage} alt="Beneficios BCP" width="300" height="300"/>
            </div>
            <div className="education-image m-3">
                <img src={beneficiosImage} alt="Beneficios BCP" width="300" height="300"/>
            </div>
        </div>
    );
}

export default LandingEducation;
