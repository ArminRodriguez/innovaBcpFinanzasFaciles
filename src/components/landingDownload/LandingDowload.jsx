import React from 'react';
import './LandingDownload.css';
import downloadApp from '../../assets/image_espacio.png';

function LandingDowload() {
    return (
        <div className="beneficios-section">
            <div className="beneficios-image">
                <img src={downloadApp} alt="Beneficios BCP" width="300" height="300" />
            </div>
            <div className="download-text">
                <h2>Descarga el App<br />
                    Banca Movil BCP
                </h2>
            </div>
            <div class="button-container">
                <a href="www.google.cl" class="download-button m-3">
                    Google Play
                </a>
                <a href="www.google.cl" class="download-button m-3">
                    App Store
                </a>
                <a href="www.google.cl" class="download-button m-3">
                    App Gallery
                </a>
            </div>
        </div>
    );
}

export default LandingDowload;
