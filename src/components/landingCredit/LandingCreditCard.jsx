import React from 'react';
import './LandingCreditCard.css';
import beneficiosImage from '../../assets/image_espacio.png';

function LandingCreditCard() {
    return (
        <div className="beneficios-section" id='beneficios'>
            <div className="beneficios-image">
                <img src={beneficiosImage} alt="Beneficios BCP" />
            </div>
            <div className="beneficios-text">
                <h2>Descubre los beneficios que tus Tarjetas de Crédito BCP traen para ti</h2>
                <p>Ahora puedes ver todos los beneficios de cada Tarjeta de Crédito Visa y American Express BCP</p>
            </div>
        </div>
    );
}

export default LandingCreditCard;
