import React from "react";
import LandingAction from "./landingAction/LandingAction";
import LandingCreditCard from "./landingCredit/LandingCreditCard";
import LandingDowload from "./landingDownload/LandingDowload";
import LandingMySpace from "./espacioBcp/LandingMySpace";
import LandingEducation from "./landingEducation/LandingEducation";
import 'bootstrap/dist/css/bootstrap.min.css';
import TopSection from "./init/landing-section";
import CustomNavbar from "./appBar/appBar";
const HomePage = () => {
    return (
        <div>
            <CustomNavbar/>
            <TopSection />
            <div className="text-above m-5">
                <h1 id="ayudas-digitales">¿Hola, ¿Qué necesitas hacer hoy?</h1>
            </div>
            <LandingAction />
            <LandingMySpace />
            <LandingCreditCard />
            <LandingEducation />
            <LandingDowload />
        </div>
    );
};

export default HomePage;