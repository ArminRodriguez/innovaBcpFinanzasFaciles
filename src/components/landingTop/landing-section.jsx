import React from 'react';
import './LandingSection.css';

function LandingSection() {
  return (
    <div className="landing-section">
      <div className="content">
        <h1>¿Te preocupa no poder<br />pagar tu próxima<br />cuota?</h1>
        <p>Si tienes un crédito efectivo o tarjeta de crédito, déjanos tus datos para ayudarte</p>
        <button className="orange-button">Ingresa aquí</button>
      </div>
    </div>
  );
}

export default LandingSection;
