import React from 'react'
import BehaviorChange from '../behavior/BehaviorChange'
import ImproveConsumption from '../improveConsumption/improveConsumption'

function Inicio() {
  return (
    <div>
      <div className="text-above">
          <h1>Cambiando mi comportamiento financiero</h1>
      </div>
      <div className="text-above">
        <p>Define un objetivo de ahorro en base a una de tus categorías de gasto </p>
      </div>
      <BehaviorChange />
      <ImproveConsumption />
    </div>
  )
}

export default Inicio