import React from 'react';
import './ImproveConsumption.css';
import { useNavigate } from 'react-router-dom';

function ImproveConsumption() {
  const navigate = useNavigate();
  return (
    <div className="improve" id='mi-espacio'>
      <div className="text-section">
        <div className="combo-box2">
          <label htmlFor="percentage-combobox">Quiero disminuir mi consumo en:</label>
          <select className="percentage-combobox" id="percentage-combobox">
            <option value="10">10%</option>
            <option value="20">20%</option>
            <option value="30">30%</option>
            <option value="40">40%</option>
            <option value="50">50%</option>
            <option value="60">60%</option>
            <option value="70">70%</option>
            <option value="80">80%</option>
            <option value="90">90%</option>
            <option value="100">100%</option>
          </select>
        </div>

        <div className="combo-box1">
          <label htmlFor="month-combobox">Por un periodo de:</label>
          <select className="month-combobox" id="month-combobox">
            <option value="1">1 mes</option>
            <option value="2">2 meses</option>
            <option value="3">3 meses</option>
            <option value="4">4 meses</option>
            <option value="5">5 meses</option>
            <option value="6">6 meses</option>
            <option value="7">7 meses</option>
            <option value="8">8 meses</option>
            <option value="9">9 meses</option>
            <option value="10">10 meses</option>
            <option value="11">11 meses</option>
            <option value="12">12 meses</option>
          </select>
        </div>
      </div>

      <div className="image-section">
        <button className="orange-button" onClick={ 
          () => navigate('/zenSpaceSeguimiento')
        }>Definir Objetivo</button>
      </div>
    </div>
  );
}

export default ImproveConsumption;
