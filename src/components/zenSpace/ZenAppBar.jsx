import React from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import Logo from '../../assets/images/bcp-logo.png';

const logoStyle = {
    marginRight: '20px',
};
const smallTextStyle = {
    fontSize: '14px',
    color: 'white',
};

const customNavbarStyle = {
    backgroundColor: '#1e2a78', 
};

function ZenAppBar() {
    return (
        <div>
            <div>
                <Navbar style={customNavbarStyle}>
                    <Container>
                        <img
                            src={Logo}
                            alt="alt"
                            width="60"
                            height="20"
                            style={logoStyle}
                        />
                        <Nav className="ms-auto gap-3">
                            <Nav.Link href="#mi-espacio" style={smallTextStyle}>
                                Aprendiendo Jugando
                            </Nav.Link>
                            <Nav.Link href="#ayudas-digitales" style={smallTextStyle}>
                                Inicio
                            </Nav.Link>
                        </Nav>
                    </Container>
                </Navbar>
            </div>
        </div>
    );
}

export default ZenAppBar;
