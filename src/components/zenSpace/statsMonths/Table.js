import React from 'react';
import monthData from './dataMonth';

const datos = [
    { mes: 'Enero', categoria: 'Restaurantes', porcentaje: 30 },
    { mes: 'Enero', categoria: 'Ropa', porcentaje: 20 },
    { mes: 'Enero', categoria: 'Entretenimiento', porcentaje: 15 },
    { mes: 'Enero', categoria: 'Hoteles', porcentaje: 10 },
    { mes: 'Febrero', categoria: 'Restaurantes', porcentaje: 25 },
    { mes: 'Febrero', categoria: 'Ropa', porcentaje: 18 },
    { mes: 'Febrero', categoria: 'Entretenimiento', porcentaje: 20 },
    { mes: 'Febrero', categoria: 'Hoteles', porcentaje: 12 },
    { mes: 'Marzo', categoria: 'Restaurantes', porcentaje: 28 },
    { mes: 'Marzo', categoria: 'Ropa', porcentaje: 22 },
    { mes: 'Marzo', categoria: 'Entretenimiento', porcentaje: 18 },
    { mes: 'Marzo', categoria: 'Hoteles', porcentaje: 15 },
    { mes: 'Abril', categoria: 'Restaurantes', porcentaje: 29 },
    { mes: 'Abril', categoria: 'Ropa', porcentaje: 19 },
    { mes: 'Abril', categoria: 'Entretenimiento', porcentaje: 17 },
    { mes: 'Abril', categoria: 'Hoteles', porcentaje: 9 },
    { mes: 'Mayo', categoria: 'Restaurantes', porcentaje: 27 },
    { mes: 'Mayo', categoria: 'Ropa', porcentaje: 21 },
    { mes: 'Mayo', categoria: 'Entretenimiento', porcentaje: 19 },
    { mes: 'Mayo', categoria: 'Hoteles', porcentaje: 11 },
    { mes: 'Junio', categoria: 'Restaurantes', porcentaje: 26 },
    { mes: 'Junio', categoria: 'Ropa', porcentaje: 23 },
    { mes: 'Junio', categoria: 'Entretenimiento', porcentaje: 20 },
    { mes: 'Junio', categoria: 'Hoteles', porcentaje: 14 },
    { mes: 'Julio', categoria: 'Restaurantes', porcentaje: 28 },
    { mes: 'Julio', categoria: 'Ropa', porcentaje: 25 },
    { mes: 'Julio', categoria: 'Entretenimiento', porcentaje: 22 },
    { mes: 'Julio', categoria: 'Hoteles', porcentaje: 10 },
    { mes: 'Agosto', categoria: 'Restaurantes', porcentaje: 27 },
    { mes: 'Agosto', categoria: 'Ropa', porcentaje: 23 },
    { mes: 'Agosto', categoria: 'Entretenimiento', porcentaje: 19 },
    { mes: 'Agosto', categoria: 'Hoteles', porcentaje: 13 },
    { mes: 'Septiembre', categoria: 'Restaurantes', porcentaje: 30 },
    { mes: 'Septiembre', categoria: 'Ropa', porcentaje: 22 },
    { mes: 'Septiembre', categoria: 'Entretenimiento', porcentaje: 18 },
    { mes: 'Septiembre', categoria: 'Hoteles', porcentaje: 12 },
    { mes: 'Octubre', categoria: 'Restaurantes', porcentaje: 29 },
    { mes: 'Octubre', categoria: 'Ropa', porcentaje: 20 },
    { mes: 'Octubre', categoria: 'Entretenimiento', porcentaje: 15 },
    { mes: 'Octubre', categoria: 'Hoteles', porcentaje: 11 },
    { mes: 'Noviembre', categoria: 'Restaurantes', porcentaje: 28 },
    { mes: 'Noviembre', categoria: 'Ropa', porcentaje: 21 },
    { mes: 'Noviembre', categoria: 'Entretenimiento', porcentaje: 17 },
    { mes: 'Noviembre', categoria: 'Hoteles', porcentaje: 9 },
    { mes: 'Diciembre', categoria: 'Restaurantes', porcentaje: 26 },
    { mes: 'Diciembre', categoria: 'Ropa', porcentaje: 24 },
    { mes: 'Diciembre', categoria: 'Entretenimiento', porcentaje: 21 },
    { mes: 'Diciembre', categoria: 'Hoteles', porcentaje: 14 },
  ];
  
  function Table({ mesSeleccionado }) {
    const datosFiltrados = datos.filter((item) => item.mes === mesSeleccionado);
    let sumaPorcentajes = 0;
    datosFiltrados.forEach((item) => {
      sumaPorcentajes += item.porcentaje;
    });
    monthData[mesSeleccionado].sumaPorcentajes = sumaPorcentajes;
    monthData.Mes=mesSeleccionado;
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Categoría</th>
              <th>Porcentaje</th>
            </tr>
          </thead>
          <tbody>
            {datosFiltrados.map((item) => (
              <tr key={item.categoria}>
                <td>{item.categoria}</td>
                <td className="centered">{item.porcentaje}%</td>
              </tr>
            ))}
          </tbody>
        </table>
        <div>
          <strong>Total Porcentajes: {monthData[mesSeleccionado].sumaPorcentajes}%</strong>
        </div>
      </div>
    );
  }
  

export default Table;
