import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';

function ChartComponent() {
  const chartRef = useRef(null);

  useEffect(() => {
    const data = {
      labels: ['', '', '', '', '', '', '', '', '', '', '','', ''],
      datasets: [
        {
          label: 'QUÉ TAN RÁPIDO GASTÉ MI DINERO?',
          data: [
            { x: '', y: 0 },
            { x: '', y: 180 },
            { x: '', y: 130 },
            {x: '', y:20},
            {x: '', y:10},
            {x: '', y:20},
            {x: '', y:120},
            {x: '', y:130},
            {x: '', y:160},
            {x: '', y:140},
            {x: '', y:0},
            {x: '', y:30},
            {x: '', y:0},
          ],
          pointBackgroundColor: ['blue', 'green', 'orange'],
        },
      ]
    };

    const options = {
      responsive: true,
      scales: {
        x: {
          type: 'category', 
        },
        y: {
          beginAtZero: true,
          max: 200, 
        },
      },
    };

    const myChart = new Chart(chartRef.current, {
      type: 'line', 
      data: data,
      options: options,
    });

    return () => {
      myChart.destroy();
    };
  }, []);

  return <canvas ref={chartRef} />;
}

export default ChartComponent;
