// month_data.js
const monthData = {
  Enero: { sumaPorcentajes: 0, categorias: [] },
  Febrero: { sumaPorcentajes: 0, categorias: [] },
  Marzo: { sumaPorcentajes: 0, categorias: [] },
  Abril: { sumaPorcentajes: 0, categorias: [] },
  Mayo: { sumaPorcentajes: 0, categorias: [] },
  Junio: { sumaPorcentajes: 0, categorias: [] },
  Julio: { sumaPorcentajes: 0, categorias: [] },
  Agosto: { sumaPorcentajes: 0, categorias: [] },
  Septiembre: { sumaPorcentajes: 0, categorias: [] },
  Octubre: { sumaPorcentajes: 0, categorias: [] },
  Noviembre: { sumaPorcentajes: 0, categorias: [] },
  Diciembre: { sumaPorcentajes: 0, categorias: [] },
};

export default monthData;
