import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Table from './Table';
import Chart from './chart';

const sectionStyle = {
  background: '#f2f2f2', 
  paddingTop: '20px',
  paddingBottom: '20px',
};

const headerStyle = {
  fontSize: '24px', 
  fontWeight: 'bold', 
  marginBottom: '20px',
  color: '#333',
};

const selectStyle = {
  fontSize: '16px',
  padding: '5px 10px',
  border: '1px solid #ccc',
  borderRadius: '5px',
};

const labelStyle = {
  fontSize: '16px',
  marginRight: '10px',
};

function StatsMonths() {
  const [mesSeleccionado, setMesSeleccionado] = useState('Enero');

  const handleMesChange = (event) => {
    setMesSeleccionado(event.target.value);
  };

  return (
    <div style={sectionStyle}>
      <Container>
        <h2 style={headerStyle}>¿Cómo se me fue el dinero en {mesSeleccionado}?</h2>
        <p>Inteligencia financiera para tu día a día</p>
        <div>
          <label htmlFor="mes" style={labelStyle}>
            Selecciona un mes:
          </label>
          <select id="mes" value={mesSeleccionado} onChange={handleMesChange} style={selectStyle}>
            <option value="Enero">Enero</option>
            <option value="Febrero">Febrero</option>
            <option value="Marzo">Marzo</option>
            <option value="Abril">Abril</option>
            <option value="Mayo">Mayo</option>
            <option value="Junio">Junio</option>
            <option value="Julio">Julio</option>
            <option value="Agosto">Agosto</option>
            <option value="Septiembre">Septiembre</option>
            <option value="Octubre">Octubre</option>
            <option value="Noviembre">Noviembre</option>
            <option value="Diciembre">Diciembre</option>
          </select>
        </div>
        <Row>
          <Col md={6}>
            <Table mesSeleccionado={mesSeleccionado}/>
          </Col>
          <Col md={6}>
            <Chart />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default StatsMonths;
