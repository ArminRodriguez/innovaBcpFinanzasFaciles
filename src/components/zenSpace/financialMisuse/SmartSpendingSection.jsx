import React from 'react';
import Space from '../../../assets/zen.png';
import './SmartSpendingSection.css';
function SmartSpendingSection() {
  return (
    <div className="landing-my-space" id='mi-espacio'>
      <div className="text-section">
      <p> Deja el  ciclo de gastos sin fin</p>
        <h2>Josué, llego tu momento Zen</h2>
        <h8 className='p-text'>Te enseñamos a usar tu dinero de manera inteligente </h8>
      </div>

      <div className="image-section">
        <img src={Space} alt="Mi Espacio BCP"  width="230"
                            height="200"/>
      </div>
    </div>
  );
}

export default SmartSpendingSection;
