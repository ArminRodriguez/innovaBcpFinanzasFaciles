import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import ZenAPP from "./ZenAppBar";
import SmartSpendingSection from "./financialMisuse/SmartSpendingSection";
import StatsMonths from "./statsMonths/StatsMonths";
import BehaviorChange from "./behavior/BehaviorChange";
import ImproveConsumption from "./improveConsumption/improveConsumption";

const HomeZen = ({ 
    component: Component,
    ...rest
}) => {
    return (
        <div>
            <ZenAPP />
            <SmartSpendingSection />
            <StatsMonths />
            {
                Component && <Component {...rest} />
            }
        </div>
    );
};

export default HomeZen;