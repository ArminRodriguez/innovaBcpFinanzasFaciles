import React from 'react'
import BehaviorChange from '../behavior/BehaviorChange'
import ImproveConsumption from '../improveConsumption/improveConsumption'
import Grafica from './Grafica'
import './Seguimiento.css'

function Seguimiento() {
  return (
    <div className="">
      <div className='text-section'>
        <div className="">
            <h1>Seguimiento de objetivos</h1>
        </div>
        <div className="">
          <p>Verifica cómo vas evolucionando en tu objetivo de ahorro</p>
        </div>
        
        <div className='d-flex gap-2 graficas'>
          <Grafica title={"Setiembre"} dataY={[1,3,5,7,10]} achieved/>
          <Grafica title={"Octubre"} dataY={[1,2,5,6]}/>
          <Grafica title={"Noviembre"} dataY={[]}/>
        </div>
      </div>

      <div className="seguimiento-info text-center d-flex justify-content-center align-items-center">
        <div className=''>
          Josué, te fijaste un objetivo máximo de <span className='font-weight-bold'>gasto de 10%</span> en <span className='font-weight-bold'>Restaurantes</span> y a la fecha has alcanzado un <spa className="font-weight-bold">5%</spa> 
        </div>
      </div>
    </div>
  )
}

export default Seguimiento