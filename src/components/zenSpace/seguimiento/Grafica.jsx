import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';
import { faCheck, faInfoCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './Grafica.css';

function Grafica({
  title,
  dataY = [],
  achieved = false,
}) {
  const chartRef = useRef(null);

  useEffect(() => {
    // Datos de ejemplo para la gráfica
    const data = {
      labels: ['', '', '', '', ''],
      datasets: [
        {
          label: 'Ventas',
          data: [...dataY],
          borderColor: 'rgba(75, 192, 192, 1)',
          fill: false,
        },
      ],
    };

    const options = {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    };

    // Crear la gráfica en el contexto del elemento canvas
    const ctx = chartRef.current.getContext('2d');
    new Chart(ctx, {
      type: 'line',
      data: data,
      options: options,
    });
  }, []);

  return (
    <div className='grafica'>
      <div className='d-flex gap-1'>
        <h2 className='mr-2'>
          {title}
          {/* Insertar un check icon de fontawesome si es que achieved es true */}
        </h2>
        {achieved && <FontAwesomeIcon className='ml-2' fontSize={30} icon={faCheck} color='green'/>}
      </div>
      <canvas ref={chartRef}></canvas>
    </div>
  );
}

export default Grafica