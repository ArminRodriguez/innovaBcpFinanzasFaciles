import React, { useEffect, useState } from 'react';
import './BehaviorChange.css';

function BehaviorChange() {
  return (
    <div className="landing-my-space">
      <div className="text-section-Chage">
       <div>  
          <label htmlFor="mes">
         <h3>Este mes has gastado un 17.5% de mi sueldo en cosas como </h3> 
          </label>
          <div  className= "combo-box" >
          <select id="mes">
            <option value="Restaurantes">Restaurantes</option>
            <option value="Ropa">Ropa</option>
            <option value="Entretenimiento">Entretenimiento</option>
            <option value="Hoteles">Hoteles</option>
          </select>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BehaviorChange;
