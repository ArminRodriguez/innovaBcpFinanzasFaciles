import React from 'react';
import { Container, Nav, Navbar, InputGroup, FormControl, Button } from 'react-bootstrap';
import { BsChevronDown } from 'react-icons/bs';
import { FaSearch } from 'react-icons/fa';
import Logo from '../assets/images/bcp-logo.png';
import '../index.css';

const logoStyle = {
    marginRight: '20px',
};

const smallTextStyle = {
    fontSize: '14px',
};

const smallTextStyleSecond = {
    fontSize: '10px',
};

const smallSearchStyle = {
    width: '120px',
    fontSize: '12px',
};

const abreCuentaButtonStyle = {
    backgroundColor: 'white',
    color: 'black',
    borderRadius: '20px',
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
    padding: '5px 10px',
    borderColor: 'gray',
    fontSize: '12px',
    marginRight: '10px',
};

const secondNavbarStyle = {
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1000',
    height: '30px', 
    backgroundColor: '#f2f8ff', 
};

const containerStyle = {
    paddingTop: '70px',
};

function CustomNavbar() {
    return (
        <div>
            <div style={containerStyle}>
                <Navbar bg="light">
                    <Container>
                        <img
                            src={Logo}
                            alt="alt"
                            width="60"
                            height="20"
                            style={logoStyle}
                        />
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto gap-3">
                                <Nav.Link href="#productos" style={smallTextStyle}>
                                    Productos <BsChevronDown />
                                </Nav.Link>
                                <Nav.Link href="servicios" style={smallTextStyle}>
                                    Servicios <BsChevronDown />
                                </Nav.Link>
                                <Nav.Link href="#ayudas-digitales" style={smallTextStyle}>
                                    Ayudas Digitales <BsChevronDown />
                                </Nav.Link>
                                <Nav.Link href="#beneficios" style={smallTextStyle}>
                                    Beneficios <BsChevronDown />
                                </Nav.Link>
                                <Nav.Link href="#nosotros" style={smallTextStyle}>
                                    Nosotros <BsChevronDown />
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>

                        <Nav className="ms-auto gap-3">
                            <InputGroup style={{ width: '160px' }}>
                                <FormControl
                                    type="text"
                                    placeholder="Buscar"
                                    style={smallSearchStyle}
                                />
                                <Button variant="outline-primary" size="sm">
                                    <FaSearch />
                                </Button>
                            </InputGroup>
                            <Button
                                variant="info"
                                size="sm"
                                style={abreCuentaButtonStyle}
                            >
                                Abre tu Cuenta
                            </Button>
                        </Nav>
                    </Container>
                </Navbar>
            </div>

            <Navbar style={secondNavbarStyle}>
                <Container>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto gap-3">
                            <Nav.Link href="#Personas" style={smallTextStyleSecond}>
                                Personas
                            </Nav.Link>
                            <Nav.Link href="#PyMES" style={smallTextStyleSecond}>
                                PyMES
                            </Nav.Link>
                            <Nav.Link href="#Empresas" style={smallTextStyleSecond}>
                                Empresas
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>

                    <Nav className="ms-auto gap-3">
                    <Nav.Link href="#idioma" style={smallTextStyleSecond}>
                                Español/Quechua
                            </Nav.Link>
                        </Nav>
                </Container>
            </Navbar>
        </div>
    );
}

export default CustomNavbar;
