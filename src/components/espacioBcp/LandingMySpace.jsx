import React from 'react';
import Space from '../../assets/image_espacio.png';
import './LandingMySpace.css';

function LandingMySpace() {
  return (
    <div className="landing-my-space" id='mi-espacio'>
      <div className="text-section">
        <h2>Mi Espacio BCP</h2>
        <p>Ingresa tu DNI y descubre tu producto especial</p>
        <div className="input-section">
          <button>DNI</button>
          <input type="text" placeholder="Nro de documento" />
        </div>
      </div>

      <div className="image-section">
        <img src={Space} alt="Mi Espacio BCP" />
      </div>
    </div>
  );
}

export default LandingMySpace;
