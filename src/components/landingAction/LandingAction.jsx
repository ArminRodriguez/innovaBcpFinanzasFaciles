import React from 'react';
import { FaExclamationTriangle } from 'react-icons/fa';
import './LandingAction.css';
import cerdo from '../../assets/cerdo.png';
function LandingAction() {
    return (
        <div className="landing-action">
            <div className="features">
                <div className="feature">
                    <a href="/login">
                        <div>
                            <img src={cerdo} alt="Icono 1" className="icon" />
                            <p>Finanzas Fáciles</p>
                        </div>
                    </a>
                </div>
                <div className="feature">
                    <img src={cerdo} alt="Icono 2" className="icon" />
                    <p>Obtener Tarjeta de Crédito</p>
                </div>
                <div className="feature">
                    <img src={cerdo} alt="Icono 3" className="icon" />
                    <p>Solicitar un Préstamo</p>
                </div>
                <div className="feature">
                    <img src={cerdo} alt="Icono 4" className="icon" />
                    <p>Adelantar mi Sueldo</p>
                </div>
                <div className="feature">
                    <img src={cerdo} alt="Icono 5" className="icon" />
                    <p>SOAT Virtual desde S/ 39.90</p>
                </div>
                <div className="feature">
                    <img src={cerdo} alt="Icono 6" className="icon" />
                    <p>Proteger mis Tarjetas</p>
                </div>
            </div>

            <div className="help-section">
                <div className="help-header">
                    <h2 ><FaExclamationTriangle className="red-light" /> Ayuda Rápida</h2>
                </div>
                <ul>
                    <li href="#">¿Cómo creo mi clave de 6 digitos?</li>
                    <li href="#">¿Cómo activar el Token Digital?</li>
                    <li href="#">¿Cómo bloquear mi Tarjeta BCP?</li>
                    <li href="#">Conoce tu Codigo de Cuenta Interbancario</li>
                </ul>
            </div>
        </div>
    );
}

export default LandingAction;
