import React from "react";
import TopSection from "./components/init/landing-section";
import LandingAction from "./components/landingAction/LandingAction";
import 'bootstrap/dist/css/bootstrap.min.css';
import AppBar from "./components/appBar/appBar";
import './components/landingAction/LandingAction.css';
import Login from "./login/login";
import HomePage from "./components/HomePage"
import Routes from "./app/routes"
const App = () => {
  return (
    <div>
      {/* <AppBar />
      <TopSection />
      <div className="text-above m-5">
      <h1 id="ayudas-digitales">¿Hola, ¿Qué necesitas hacer hoy?</h1>
      </div>
      <LandingAction /> */}
      <Routes/>
    </div>
  );
};

export default App;