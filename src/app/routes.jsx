import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'; 
import React from 'react';
import HomePage from '../components/HomePage';
import HomeZen from '../components/zenSpace/homeZen';
import Login from '../login/login';
import Inicio from '../components/zenSpace/inicio/Inicio';
import Seguimiento from '../components/zenSpace/seguimiento/Seguimiento';
export default function AppRoutes() {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<HomePage/>} />
                <Route path="/zenSpace" element={<HomeZen component={ Inicio } />} />
                <Route path="/zenSpaceSeguimiento" element={<HomeZen component={ Seguimiento }/>} />
                <Route path="/login" element={<Login/>} />
            </Routes>
        </Router>
    );
}